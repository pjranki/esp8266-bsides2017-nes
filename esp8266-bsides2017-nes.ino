#include <SPI.h>
#include <user_interface.h>
#include <Adafruit_ST7735.h>
#include <crc32fast.h>
#include <nes.h>

// NES game
//  - BUFFER_SIZE
//  - const char BUFFER[BUFFER_SIZE] PROGMEM
#include "nes_smb3.h"  // generate this using the rom_to_header.py script in nes library

// ESP8266
#define CPU_FREQUENCY   160 // Mhz (valid 80, 160)
#define SPI_FREQUENCY   24  // Mhz (don't go above 40 Mhz)

// ST7735 screen wiring on Badge
#define TFTDC   4   // D/C select line to TFT
#define TFTCS   15  // Active LOW to TFT
#define TFTBL   5   // Active HIGH to turn on TFT backlight
#define TFTRST  16  // No pin assigned to TFT reset

// ST7735 screen
Adafruit_ST7735 tft = Adafruit_ST7735(TFTCS, TFTDC, TFTRST);

// Draw a line of pixels fast
inline void drawFastHLineRgb565(uint16_t x0, uint16_t y0, uint16_t width, uint16_t *color)
{
  uint16_t current_color = 0;
  uint16_t last_pixel_match = 0;
  uint16_t new_color = 0;
  uint16_t window = 0;
  
  for (uint16_t pixel = 0; pixel < width; pixel++)
  {
    new_color = color[pixel];
    if (current_color != new_color)
    {
      window = pixel-last_pixel_match;
      tft.drawFastHLine(last_pixel_match, y0, window, current_color);
      current_color = new_color;
      last_pixel_match = pixel;
    }
  }
  window = (width-1)-last_pixel_match;
  tft.drawFastHLine(last_pixel_match, y0, window, current_color);
}

// NES
nes::Emulator nes_game;

// NES graphics (average odd and even lines, only draw on change)
static uint16_t g_last_line[nes::SCREEN_WIDTH] = {0};
static uint16_t g_render_line[nes::SCREEN_WIDTH>>1] = {0};
static uint32_t g_last_line_sum = 0;
static uint32_t g_line_sum[nes::SCREEN_HEIGHT>>1] = {0};

void setup() {
  // Serial
  Serial.begin(115200);
  while (!Serial);
  Serial.println("Serial ready");

  // ST7735 screen
  pinMode(TFTDC, OUTPUT);
  pinMode(TFTCS, OUTPUT);  
  pinMode(TFTBL, OUTPUT);
  digitalWrite(TFTBL, HIGH);
  tft.initR(INITR_GREENTAB);
  tft.setRotation(4);
  tft.fillScreen(ST7735_BLACK);
  delay(1000);

  // NES
  nes_game.set_render_line([](uint32_t y, const nes::palindex_t *line, uint32_t line_size, const nes::rgb16_t *palette) -> void
  {
    if ((y & 1) == 0)
    {
      // store and checksum odd-numbered lines
      for (uint32_t pixel; pixel < line_size; pixel++)
      {
        g_last_line[pixel] = palette[line[pixel]];
      }
      g_last_line_sum = crc32fast::calculate(g_last_line, sizeof(g_last_line));
    }
    else
    {
      uint32_t line_sum = g_last_line_sum;
      line_sum = crc32fast::calculate(line, line_size, line_sum);
      line_sum = crc32fast::calculate(palette, nes::PALLET_SIZE_SELECTED * sizeof(nes::rgb16_t), line_sum);
        
      if (g_line_sum[y>>1] != line_sum)
      {
        g_line_sum[y>>1] = line_sum;
        for (uint32_t pixel = 0; pixel < line_size; pixel += 2)
        {
          if ((pixel>>1) < (sizeof(g_render_line) >> 1))
          {
            g_render_line[pixel>>1] = NES_RGB565_AVERAGE_4_VAL(palette[line[pixel]], palette[line[pixel+1]], g_last_line[pixel], g_last_line[pixel+1]);
          }
        }
        drawFastHLineRgb565(0, (uint16_t)(y>>1), (sizeof(g_render_line) >> 1), g_render_line);
      }
    }
  });
  nes_game.set_frame_end([]() {
    wdt_reset();
  });
  nes_game.set_log([](const char *message){
    Serial.print("log:nes:");
    Serial.print(message);
  });
  nes_game.set_memcpy([](void *dst, const void *src, uint32_t size) {
    memcpy_P(dst, src, size);
  });

  // ESP9266
  system_update_cpu_freq(CPU_FREQUENCY); 
  SPI.setFrequency(SPI_FREQUENCY*1000000);

  // NES reset and load
  nes_game.reset();
  nes_game.load(BUFFER, BUFFER_SIZE);
}

void loop() {
  nes_game.tick();
}

